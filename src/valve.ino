#include "WS.h"
void initValve(int pin)
{
  pinMode(pin, OUTPUT);
}

void turnOnValve(int pin)
{
  digitalWrite(pin, HIGH);
}

void turnOffValve(int pin)
{
  digitalWrite(pin, LOW);
}

