#define DEBUG 1
#include<stdio.h>
#include"SD.h"
#include"LCD.h"
#include"RTClib.h"
#include"WS.h"

typedef struct {
	bool (*df)(char *, byte);
	byte row;
	byte col;
	byte size;
	} df_type;

df_type dfl[4];

// void (*dflist[4])(void);
int numdfs = 0;

void addDF(bool (*df)(char *, byte), byte row, byte col, byte size)
{
  dfl[numdfs].df = df;
  dfl[numdfs].row = row;
  dfl[numdfs].col = col;
  dfl[numdfs].size = size;
  numdfs++;
}

void clearDFs()
{
  numdfs = 0;
}

void callDFs()
{
  char buf[5];

  LOG("Number of Dynamic Functions: ");LOGLN(numdfs);
  for (int i=0;i<numdfs;i++) {
    if (dfl[i].df(buf, dfl[i].size)) {
      LCDSetCursor(dfl[i].row,dfl[i].col);
      LCDPrint(buf,dfl[i].size); 
    }
  }
}

bool writeCountDown(char *b, byte sz)
{
  static int currCount = 0;

  int newCount = cdTimer.read();

  LOG("In WriteCountDown(): newCount = ");LOG(newCount);
  LOG(" currCount = ");LOGLN(currCount);

  if (newCount != currCount) {
    int len=0, x=newCount; while(x>0) {x/=10;len++;} //Counts the num of digits
    int p = 0; while(len+p < sz){b[p]='0';p++;} //Pads front with 0s

    currCount = newCount;
    sprintf(&b[p],"%d",currCount);
    LOGLN(b);
    return(true);
  }
  return(false);
}
#define DATAFILENAME "DAT/DAT.DAT"

#define MAXSTATS 10

extern RTC_DS1307 rtc;

int numFileTableEntries=0;
dataStruct_t statsTable[MAXSTATS];
fStruct_t fileTable[9];
uint8_t numStats;
char currDirectory[20];

CountDownTimer cdTimer;

void resetFileTable()
{
  numFileTableEntries=0;
}

void processFileTable(int command)
{
  static int currFileEntry = 0;
  static unsigned long timr = 0UL;
  char buf[30]="";
  unsigned long now=millis();

  if (numFileTableEntries == 0) return;

  switch (command) {
    case CMD_START:
      currFileEntry=-1;
      timr = now;

    case CMD_UPDATE:
      if (((long)now - (long)timr) >= 0) {
        currFileEntry++;
        if (currFileEntry == numFileTableEntries) currFileEntry=0;
        timr = now + (unsigned long)(1000*fileTable[currFileEntry].seconds);

        sprintf(buf,"%s%s",currDirectory,fileTable[currFileEntry].file);
        processStateFile(buf);
      }
        
      LOG(":");LOG(now);LOG(":");LOG(timr);LOG(":CURR FILE:");LOGLN(currFileEntry);
      break;

    case CMD_RESUME:
      
      break;
    case CMD_STOP:
      break;
  }
}

void updateScreen()
{
  LOG(",");LOG(numFileTableEntries);
  processFileTable(CMD_UPDATE);

  callDFs();
}

void processStateFile(char *stateFile)
{
  char buf[80] = "hello World";

  LOGLN(stateFile);
  File state = SD.open(stateFile);
  
  while(state.available()) {
    char splitChar[2];
    splitChar[0] = state.read();
    splitChar[1] = '\0';

    for (int i=0; i<80; i++) {
      buf[i] = state.read();
      if (buf[i] == '\n') {
        buf[i] = '\0';
        break;
      }
    }

    //LOGLN(buf);
    processConfigLine(buf, splitChar);

  }
  //Serial.print("test ");
  //Serial.println(strlen(buf));
  //Serial.println(buf);
  state.close();
  
}

void processConfigLine(char *lin, char *sc)
{
  char *token;
  
    char *id = strtok(lin, sc);
    switch (id[0]) {
      int line, col, num;
      char *str;
      case 'S':
        token = strtok(NULL,sc);
        line = atoi(token);
        token = strtok(NULL,sc);
        col = atoi(token);
        str = strtok(NULL,sc); 
        token = strtok(NULL,sc);
        num = atoi(token);

        LCDSetCursor(line,col);
        LCDPrint(str,num); 
        //LOG("Line:");LOG(line);
        //LOG(" Column:");LOG(col);
        //LOG(" String:");LOGLN(str);
        break;

      case 'D':
        token = strtok(NULL,sc);
        line = atoi(token);
        token = strtok(NULL,sc);
        col = atoi(token);
        str = strtok(NULL,sc); 
        token = strtok(NULL,sc);
        num = atoi(token);
        
        sprintf(lin,"%d", getDataValue(str,statsTable, numStats));
        LCDSetCursor(line,(col+num-strlen(lin)));
        LCDPrint(lin,num); 
        break;

      case 'F':
        token = strtok(NULL,sc);
        num = atoi(token);
        for (int i=0;i<num;i++) {
          token = strtok(NULL,sc);
          strncpy(fileTable[i].file, token, 11);
          token = strtok(NULL,sc);
          fileTable[i].seconds = atoi(token);
        }
        numFileTableEntries = num;
        break;

      case 'C':
        token = strtok(NULL,sc);
        line = atoi(token);
        token = strtok(NULL,sc);
        col = atoi(token);
        token = strtok(NULL,sc); // for future use
        token = strtok(NULL,sc);
        num = atoi(token);
        addDF(writeCountDown, (byte)line, (byte)col, (byte)num);
        
        break;
      case 'c':
        token = strtok(NULL,sc);
        cdTimer.set(atoi(token));
        break;

      case 't':
      case 'd':
        token = strtok(NULL,sc);
        line = atoi(token);
        token = strtok(NULL,sc);
        col = atoi(token);
        DateTime now = rtc.now();

        LCDSetCursor(line,col);
        switch (id[0]) {
          char cbuf[5];
          case 't':
            sprintf(cbuf,"%02d:",(now.hour()<12)?now.hour():now.hour()-12);
            LCDPrint(cbuf,3); 
            sprintf(cbuf,"%02d ",now.minute());
            LCDPrint(cbuf,3); 
            sprintf(cbuf,"%s",(now.hour()<12)?"AM ":"PM "); 
            LCDPrint(cbuf,3); 
            break;
          case 'd':
            sprintf(cbuf,"%02d-",now.month());
            LCDPrint(cbuf,3); 
            sprintf(cbuf,"%02d-",now.day());
            LCDPrint(cbuf,3); 
            sprintf(cbuf,"%04d",now.year()); 
            LCDPrint(cbuf,4); 
            break;
        }
        break;
    }
}

void writeDataToFile(char *fileName, dataStruct_t *dataTable, uint8_t numFields)
{
  char buf[20] = "";
  
  File data = SD.open(fileName);

  for (int i=0;i < numFields;i++) {
    sprintf(buf,":%s:%d:\n", dataTable[i].name, dataTable[i].value);
    data.print(buf);
  }
  data.close();
}

int readDataFromFile(char *fileName, dataStruct_t *DataTable) 
{
  char buf[20] = "";
  char *token;
  int ndx = 0;
  File data = SD.open(fileName);

  while(data.available()) {
    char splitChar[2];
    splitChar[0] = data.read();
    splitChar[1] = '\0';

    for (int i=0; i<20; i++) {
      buf[i] = data.read();
      if (buf[i] == '\n') {
        buf[i] = '\0';
        break;
      }
    }

    char *nam = strtok(buf, splitChar);
    strcpy(DataTable[ndx].name, nam);
    token = strtok(NULL, splitChar);
    DataTable[ndx].value = atol(token);

    LOG(ndx);LOG(", ");LOG(DataTable[ndx].name); LOG(":");LOGLN(DataTable[ndx].value);
    ndx++;

  }

  data.close();
  return (ndx);
}

long getDataValue(char *dat, dataStruct_t *dataTable, uint8_t tSize)
{
  for (int i=0; i<tSize;i++) {
    if (strncmp(dataTable[i].name, dat, 7) == 0) {
      return (dataTable[i].value);
    }
  }
  return (0);
}

int initSDFiles(char *topDir)
{
  char buf[20]="";

  pinMode(10,OUTPUT);

  if (!SD.begin(10,11,12,13)) return (-1);

  // Setup currDirectory variable to current directory we are working in.
  currDirectory[0]=NULL;
  strcat(currDirectory, topDir);
  strcat(currDirectory,"FSM/");
  LOGLN(currDirectory);

  strcat(buf, topDir);
  strcat(buf, DATAFILENAME);

  File data = SD.open(buf);
  numStats = readDataFromFile(buf, statsTable);
  
  return (0);
}
