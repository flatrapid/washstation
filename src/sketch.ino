#include <Wire.h>  
#include "LCD.h" 
#include "RTClib.h" 
#include "SD.h"
#include "WS.h"
#include "FiniteStateMachine.h"
#include "state.h"

#define PROJ "TEST"
#define DATADIRNAME "DAT"
#define CFGDIRNAME "CFG"
#define FSMDIRNAME "FSM"

RTC_DS1307 rtc;

State idle(idleEnter,idleUpdate,idleExit);
State wash(washEnter,washUpdate,washExit);
State compliant(compliantEnter,compliantUpdate,compliantExit);
State nonCompliantWarn(nonCompliantWarnEnter,nonCompliantWarnUpdate,nonCompliantWarnExit);
State nonCompliantFail(nonCompliantFailEnter,nonCompliantFailUpdate,nonCompliantFailExit);

FiniteStateMachine fsm(idle);

inline void dirname(char *buf, char *proj, char *dir) 
{
  sprintf(buf,"/%s/%s/",proj,dir);
}

char *test();

void setup()
{
  Serial.begin(9600);

  initValve(WS_VALVE_WATER);
  initValve(WS_VALVE_SOAP);


  LCDInit();

  initSDFiles("/TEST/");

  rtc.begin();
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(__DATE__, __TIME__));
    Serial.println(__DATE__);
    Serial.println(__TIME__);
  }

/*
  Serial.begin(57600);
#ifdef AVR
  Wire.begin();
#else
  Wire1.begin(); // Shield I2C pins connect to alt I2C bus on Arduino Due
#endif

    // rtc.adjust(DateTime(__DATE__, __TIME__));
*/
}

//*******************************************************************************************************************
//								                                        MAIN LOOP
//*******************************************************************************************************************

void loop()
{

  fsm.update();
  Serial.print(".");
  delay(100);
/*
  static int count = 99;
static char buf[80];
  // LCDClear();
  
  sprintf(buf,"%d",count);
  LCDSetCursor(1,0); 
  LCDPrint(buf,2);
  LCDSetCursor(1,3); 
  LCDPrint("SECONDS REMAINING", 17);
  LCDSetCursor(2,5); 
  LCDPrint("There boy this sucks", 8);
  LCDSetCursor(1,12); 
  LCDPrint("World", 8);

  delay(1000);
  count--;

  if (count < 0) count =99;
*/
}

