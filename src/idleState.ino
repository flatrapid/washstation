#define DEBUG 1

#include "FiniteStateMachine.h"
#include "state.h"
#include "WS.h"


void idleEnter()
{
  LOGLN("idleEnter()");

  resetFileTable();
  processStateFile("/TEST/FSM/S1.TXT");

  processFileTable(CMD_START);
}

void idleUpdate()
{
  LOGLN("idleUpdate()");
  updateScreen();

  if (checkIRState() == IR_HANDS_IN) {
    fsm.transitionTo(wash);
  }

}

void idleExit()
{
}
