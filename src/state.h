extern void idleEnter();
extern void idleUpdate();
extern void idleExit();

extern void washEnter();
extern void washUpdate();
extern void washExit();

extern void nonCompliantWarnEnter();
extern void nonCompliantWarnUpdate();
extern void nonCompliantWarnExit();

extern void nonCompliantFailEnter();
extern void nonCompliantFailUpdate();
extern void nonCompliantFailExit();

extern void compliantEnter();
extern void compliantUpdate();
extern void compliantExit();

extern FiniteStateMachine fsm;
extern State idle;
extern State nonCompliantWarn;
extern State nonCompliantFail;
extern State wash;
extern State compliant;

