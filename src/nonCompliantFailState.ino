#include "FiniteStateMachine.h"
#include "state.h"
#include "sensor.h"
#include "WS.h"

void nonCompliantFailEnter()
{
  LOGLN("nonCompliantFailEnter()");

  resetFileTable();
  processStateFile("TEST/FSM/S5.TXT");

}

void nonCompliantFailUpdate()
{
  LOGLN("nonCompliantFailUpdate()");
  updateScreen();

  if (fsm.timeInCurrentState() > 5000UL) {
    fsm.transitionTo(idle);
  }

}

void nonCompliantFailExit()
{
}
