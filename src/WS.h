#include "sensor.h"
#include "valve.h"
#include "CountDownTimer.h"

#define CMD_START 1
#define CMD_UPDATE 2
#define CMD_STOP 3
#define CMD_RESUME 4

#define WS_VALVE_WATER (4)  // Control pin for Water Valve
#define WS_VALVE_SOAP (5)  // Control pin for Soap & Water Valve

#define DEBUG 1
#ifdef DEBUG
#define LOG(x) Serial.print(x)
#define LOGLN(x) Serial.println(x)
#else
#define LOG(x) 
#define LOGLN(x) 
#endif

typedef struct {
	long value;
	char name[8];
} dataStruct_t;

typedef struct {
	char file[12];
	int seconds;
} fStruct_t;

extern CountDownTimer cdTimer;

// void setCountDownTimer(int);
// int readCountDownTimer();

void clearDFs();

int initSDFiles(char *);
void processStateFile(char *);
void processConfigLine(char *, char *);
long  getDataValue(char *, dataStruct_t *, uint8_t);
void updateScreen();
void update();
void resetFileTable();
void processFileTable(int);
