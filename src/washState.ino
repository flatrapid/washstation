#define DEBUG 1

#include "FiniteStateMachine.h"
#include "state.h"
#include "sensor.h"
#include "WS.h"
#include "CountDownTimer.h"

#define WS_WET 1
#define WS_LATHER 2
#define WS_RINSE 3

int washState; 
int trans1, trans2;

struct {
  unsigned long timeIn;
  unsigned long cdTime;
} resumeState = { 0, 0 };

void returnState()
{
  cdTimer.setMillis(resumeState.cdTime);
}

void saveState()
{
  resumeState.timeIn += fsm.timeInCurrentState();
  resumeState.cdTime = cdTimer.readMillis(); 
}

void initializeState()
{
  resumeState.timeIn = 0L;
}
void washEnter() 
{
  LOGLN("WashEnter()");

  resetFileTable();
  if (&idle == &(fsm.getPreviousState())) { 
    initializeState();

    processStateFile("/TEST/FSM/S2.TXT");

    processFileTable(CMD_START);

    washState = WS_WET;

    trans1 = 27;
    trans2 = 10;

    // Turn on Valve 1
    turnOnValve(WS_VALVE_WATER);
  } else {
    processStateFile("TEST/FSM/S2-1.TXT");
    processFileTable(CMD_RESUME);
    returnState();
  }
}

void washUpdate()
{
  LOGLN("WashUpdate()");
  updateScreen();

  int countDown = cdTimer.read();

  if (checkIRState() != IR_HANDS_IN) {
    fsm.transitionTo(nonCompliantWarn);
    saveState();
  }

  switch (washState) {
    case WS_WET:
      if (countDown < trans1) {
        washState = WS_LATHER;
        // turn off valve 1 and turn on Valve 2
        turnOffValve(WS_VALVE_WATER);
        turnOnValve(WS_VALVE_SOAP);
      }
      break;
    case WS_LATHER:
      if (countDown < trans2) {
        washState = WS_RINSE;
        // turn off valve 2 and turn on Valve 1
        turnOffValve(WS_VALVE_SOAP);
        turnOnValve(WS_VALVE_WATER);
      }
      break;
    case WS_RINSE:
      if (countDown <= 0) {
        turnOffValve(WS_VALVE_WATER);
      }
      break;
  }
  LOG("COUNTDOWN : ");LOGLN(countDown);

  if (countDown <= 0) {
    fsm.transitionTo(compliant);
  }
}

void washExit()
{
  clearDFs();
}
