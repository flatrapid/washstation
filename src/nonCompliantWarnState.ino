#include "FiniteStateMachine.h"
#include "state.h"
#include "sensor.h"
#include "WS.h"

void nonCompliantWarnEnter()
{
  LOGLN("nonCompliantWarnEnter()");

  resetFileTable();
  processStateFile("TEST/FSM/S4.TXT");
}

void nonCompliantWarnUpdate()
{
  LOGLN("nonCompliantWarnUpdate()");
  updateScreen();

  if (checkIRState() == IR_HANDS_IN) {
    fsm.transitionTo(wash);
  }

  if (fsm.timeInCurrentState() > 5000UL) {
    fsm.transitionTo(nonCompliantFail);
  }
}

void nonCompliantWarnExit()
{
  LOGLN("nonCompliantWarnExit()");
}
