#define DEBUG 1

#include "FiniteStateMachine.h"
#include "state.h"
#include "sensor.h"
#include "WS.h"

void compliantEnter()
{
  LOGLN("compliantEnter()");
  resetFileTable();
  processStateFile("/TEST/FSM/S3.TXT");

  processFileTable(CMD_START);
}

void compliantUpdate()
{
  LOGLN("compliantUpdate()");
  updateScreen();

  if (fsm.timeInCurrentState() > 5000UL) {
    fsm.transitionTo(idle);
  }
}

void compliantExit()
{
}
