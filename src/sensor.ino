#include "sensor.h"
#include "WS.h"

#define IR_SENSOR_PIN A1
#define IR_THRESHOLD 250

void initSensors()
{
  initTempSensor();
}

int readAnalogSensor(int pin)
{
  return(analogRead(pin));
}

#define NUM_IR_READINGS 5
int checkIRState()
{
  static long lastRead=0;
  static long timeOutofRange=0;
  static int currState = IR_NO_HANDS;
  static int currReading = 0;
  static int ptr=0;
  static int readArray[NUM_IR_READINGS] = {0,0,0,0,0};

  int IRreading;
  int sum;
  long now = millis();

  if (50L < (now - lastRead)) { //Time to read sensor  
    lastRead = now;
    int x = analogRead(A1);
    readArray[ptr]=x;
    ptr++;
    if (NUM_IR_READINGS == ptr) ptr = 0;

    for (int i=0; i<NUM_IR_READINGS;i++) {
      sum += readArray[i];
    }
    int mean = (int)(sum/NUM_IR_READINGS);

    IRreading = mean;
    LOG("IR SENSOR: ");LOGLN(IRreading);
  } else {
    IRreading = currReading;
  }
 
/*************************
  if (IR_NO_HANDS==currState) {
    if (IRreading > IR_THRESHOLD) {
ng
      currState = IR_HANDS_IN;
    }
  } else {
    if (IRreading > IR_THRESHOLD) {  // Hands are out of range
      if (currReading == IR_HANDS_IN) {
        timeOutofRange = millis();
      } else {
        if ((millis() - timeOutofRange) >1000) {
          currState = IR_NO_HANDS;
        }
      }
    }
  }
***************/
  currReading = (IRreading>IR_THRESHOLD) ? IR_HANDS_IN : IR_NO_HANDS;
//  return (currState);
  return (currReading);
}

void initTempSensor() 
{
}

int readTempSensor() 
{
  return(94);
}
