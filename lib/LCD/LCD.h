void LCDInit();
void LCDClear();
void LCDHome();
void LCDPrintAt(uint8_t line, uint8_t col, char *str, uint8_t maxChar);
void LCDPrint(char *str, uint8_t maxChar);
void LCDSetCursor(uint8_t line, uint8_t col);
