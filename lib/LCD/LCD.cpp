#include <Wire.h>   

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif


// Port Expander
#define	MCP23017 B00100000 //MCP23017 I2C Address was B01000000
//                     xxx <- These are the I2C address bits

#define	 IOCON	 0x0A	 // MCP23017 Config Reg.
#define	 IODIRA	 0x00	 // MCP23017 address of I/O direction
#define	 IODIRB	 0x01	 // MCP23017 1=input

#define	 IPOLA	 0x02	 // MCP23017 address of I/O Polarity
#define	 IPOLB	 0x03	 // MCP23017 1= Inverted

#define	 GPIOA	 0x12	 // MCP23017 address of GP Value
#define	 GPIOB	 0x13	 // MCP23017 address of GP Value

#define	 GPINTENA 0x04	 // MCP23017 IOC Enable
#define	 GPINTENB 0x05	 // MCP23017 IOC Enable

#define	 INTCONA  0x08	 // MCP23017 Interrupt Cont 
#define	 INTCONB  0x09	 // MCP23017 1= compair to DEFVAL(A or B) 0= change

#define	 DEFVALA  0x06	 // MCP23017 IOC Default value
#define	 DEFVALB  0x07	 // MCP23017 if INTCONA set then INT. if diff. 

#define	 GPPUA	 0x0C	 // MCP23017 Weak Pull-Ups
#define	 GPPUB	 0x0D	 // MCP23017 1= Pulled HIgh via internal 100k

// LCD
#define	 ClrLCD	 0x01	 // clear the LCD
#define	 CrsrHm	 0x02	 // move cursor to home position
#define	 CrsrLf	 0x10	 // move cursor left
#define	 CrsrRt	 0x14	 // move cursor right
#define	 DispLf	 0x18	 // shift displayed chars left
#define	 DispRt	 0x1C	 // shift displayed chars right
#define	 DDRam	 0x80	 // Display Data RAM control
#define	 ddram2	 0xC0	 // 9th position of display (not in next 

#define	 RS_pin	 B00000100
#define	 RW_pin	 B00000010
#define	 E_pin	 B00000001


//*******************************************************************************************************************
//								                                   VARIABLE INITS
//*******************************************************************************************************************

int LCDCONT = 0;        // REQUIRED
byte button = 0;            // REQUIRED
int  i  =0;                 // REQUIRED

int onoffval =0;    

boolean backlightStatus = false;    // Back Lighting on or off
  
//unsigned long Starttime;            // Timer for Dispay Sleep
//unsigned long Counttime;            // Timer for Dispay Sleep
//#define sleeptimmer 20000           // Delay before display backlight sleep

int buttonPress    =0;
int count  = 0;

// SIZER        "0123456789012345678901234567890123456789"
//char mess[16] = "I2C LCD ";  // Hello style it's working message
//char mess1[16] ="Button: ";  // Part of the "a button is being pressed" message
//char NUMB[6] =  "00000";

// REQUIRED     I2C TX RX
void I2C_TX(byte device, byte regadd, byte tx_data)                              // Transmit I2C Data
{
  Wire.beginTransmission(device);
  Wire.write(regadd); 
  Wire.write(tx_data); 
  Wire.endTransmission();
}

void I2C_RX(byte devicerx, byte regaddrx)                                       // Receive I2C Data
{
  Wire.beginTransmission(devicerx);
  Wire.write(regaddrx); 
  Wire.endTransmission();
  Wire.requestFrom(int(devicerx), 1);   

  byte c = 0;
  if(Wire.available())
  {
    byte c = Wire.read();    
    c = c & 0x7F;                                                               // New: removed button 5 (now used for backlight)
    button = c >>3;
  }
}

// REQUIRED   PORTEXPANDER INIT
void portexpanderinit()
{
  // --- Set I/O Direction
  // old I2C_TX(MCP23017,IODIRB,B11111000);
  I2C_TX(MCP23017,IODIRB,B01111000); // New: removed button 5 (now used for backlight)
  //
  I2C_TX(MCP23017,IODIRA,B00000000);
  //  --- Set I/O Polarity
  I2C_TX(MCP23017,IPOLA,B00000000);
  //I2C_TX(MCP23017,IPOLB,B11111000);
  I2C_TX(MCP23017,IPOLB,B01111000);
  //  --- Set ALL Bits of GPIOA
  I2C_TX(MCP23017,GPIOA,B00000000);
  // --- Set Weak Pull-Up on Bits 7 of GPIOB
 // old I2C_TX(MCP23017,GPPUB,B11111000);
  I2C_TX(MCP23017,GPPUB,B01111000); // New: removed button 5 (now used for backlight)
  // --- Set Default on Bits 7 of GPIOB
  I2C_TX(MCP23017,DEFVALB,B00000000);
  // --- Set Use Default on Bits 7 of GPIOB
  I2C_TX(MCP23017,INTCONB,B10000000);
  // --- Set IOC on Bits 7 of GPIOB
  I2C_TX(MCP23017,GPINTENB,B10011000);
  // --- Set active low of int pin
  I2C_TX(MCP23017,IOCON,B00110000);
}

// REQUIRED      LCD WRITE / COMMAND
void LCDwr(byte lcdChar)
{
  I2C_TX(MCP23017,GPIOA,lcdChar);
  LCDCONT = LCDCONT | E_pin | onoffval;      // If RS is set then it stays set
  delayMicroseconds(40);
//  LCDCONT = LCDCONT | onoffval;
  I2C_TX(MCP23017,GPIOB,LCDCONT);
  LCDCONT = RS_pin | onoffval;
  I2C_TX(MCP23017,GPIOB,LCDCONT);
}

void LCDcmd(byte cmdlcd)
{
  LCDCONT =0;                         // was bcf RS_pin
  LCDCONT = LCDCONT | onoffval;
  I2C_TX(MCP23017,GPIOB,LCDCONT);
  LCDwr(cmdlcd);
  delayMicroseconds(40);
}


//  READ IF BUTTONS ARE BEING PRESSED
void checkbutton()  // Use serial monitor at 9600bps to see buttons that are pressed
{
  I2C_RX(MCP23017,GPIOB);
  buttonPress = int(button);
  switch (buttonPress)                        
  {
  case 1:
  button = 1;
    break;
    
  case 2:
  button = 2;
    break;
    
  case 4:
  button = 3;
    break;
    
  case 8:
  button = 4;
    break;
    
   // case 16:                                       // There is no button "5" while using firmware controled back-lighting
   //   button = 5;
   //  break; 
  }
}

void backlight(boolean onoff)
{
  onoffval =0;
  LCDCONT = LCDCONT & 0x7F;                           // Used to mask the port expander port used for LCD control
  if(onoff == true)
  {
   onoffval =0x80;
  }
  
  LCDCONT = LCDCONT | onoffval;
  I2C_TX(MCP23017,GPIOB,LCDCONT);
}

// REQUIRED  LCD INIT
void lcdinit()
{
  // Only used with port expander
  LCDCONT = RS_pin | E_pin | onoffval;
  I2C_TX(MCP23017,GPIOB,LCDCONT);

  delay(50);
  LCDcmd(B00110000); // Standard Hitachi initialization for 8-bit mode form sepc sheets
  delay(60);
  LCDcmd(B00110000);    
  delay(60);
  LCDcmd(B00110000);      
  delay(60);
  LCDcmd(B00111000);      
  delay(60);
  LCDcmd(B00001000);        
  delay(60);
  LCDcmd(ClrLCD);          
  delay(60);
  LCDcmd(B00000110); 	
  delay(60);
  LCDcmd(B0001100);        	
  delay(60);
}

void LCDSetCursor(uint8_t line, uint8_t col)
{
  static int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };

  LCDcmd(0x80 | row_offsets[line] + col);
}

void LCDPrint(char *str, uint8_t maxChar)
{
  for (int i=0; i<maxChar; i++) {
    if (NULL != str[i])
      LCDwr(str[i]);
    else
      break;
  }
}

void LCDClear()
{
  LCDcmd(ClrLCD);
  delayMicroseconds(1500);
}

void LCDInit()
{
  Wire.begin();      // join i2c bus (address optional for master)
  portexpanderinit();
  delay(200);
  lcdinit();
  delay(500);
  LCDClear();
    
  backlightStatus = true;
  backlight(backlightStatus);
  
}
