/*
 * Copyright (c) 2014 by Flat Rapids Innovation
 */

#ifndef _COUNTDOWNTIMER_H
#define _COUNTDOWNTIMER_H

#include <Arduino.h>

class CountDownTimer{
public:
  inline int read();
  inline void set(int);
  inline void pause();
  inline void resume();
  inline unsigned long readMillis();
  inline void setMillis(long);

private:
  int cdSecs;
  long cdStart;
  long pauseTime;
};

int CountDownTimer::read() {
  if (0L == pauseTime) {
    return( cdSecs - (int)((millis()-cdStart)/1000) );
  } else {
    return (cdSecs - (int)((pauseTime - cdStart)/1000));
  }
}

unsigned long CountDownTimer::readMillis() {
  if (0L == pauseTime) {
    return( (long)cdSecs*1000 - (millis()-cdStart) );
  } else {
    return ((long)cdSecs*1000 - (pauseTime - cdStart));
  }
}

void CountDownTimer::set(int numSecs) {
  cdStart = millis();
  cdSecs = numSecs;
  pauseTime = 0L;
}

void CountDownTimer::setMillis(long numMillis) {
  cdStart = millis() - numMillis%1000;
  cdSecs = numMillis/1000;
  pauseTime = 0L;
}

void CountDownTimer::pause () {
  pauseTime = millis();
}

void CountDownTimer::resume() {
  cdStart += (millis() - pauseTime);
  pauseTime = 0L;
}

#endif
